import React, { useEffect, useState } from 'react';

function ConferenceForm() {
  const [name, setName] = useState('');
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }
  const [starts, setStarts] = useState('');
  const handleStartChange = (event) => {
    const value = event.target.value;
    setStarts(value);
  }
  const [ends, setEnds] = useState('');
  const handleEndChange = (event) => {
    const value = event.target.value;
    setEnds(value);
  }
  const [description, setDescription] = useState('');
  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  }
  const [maxPresentations, setMaxPresentations] = useState('');
  const handlemaxPresentationsChange = (event) => {
    const value = event.target.value;
    setMaxPresentations(value);
  }
  const [maxAttendees, setMaxAttendees] = useState('');
  const handleMaxAttendeesChange = (event) => {
    const value = event.target.value;
    setMaxAttendees(value);
  }

  const [location, setLocation] = useState('');
  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  }

  const [locations, setLocations] = useState([]);
  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations)
    }
  }


    const handleSubmit = async (event) => {
      event.preventDefault();

      // create an empty JSON object
      const data = {};

      data.name = name;
      data.starts = starts;
      data.ends = ends;
      data.description = description;
      data.maxPresentations = maxPresentations;
      data.maxAttendees = maxAttendees;
      data.locations = locations;

      console.log(data);
      const Url = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(Url, fetchConfig);
      if (response.ok) {
        const newConference = await response.json();
        console.log(newConference);
        setName('');
        setStarts('');
        setEnds('');
        setDescription('');
        setMaxPresentations('');
        setMaxAttendees('');
        setLocation('');
      }
    }


  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new location</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
            <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" value={name} id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStartChange} placeholder="starts" required type="datetime-local" name="starts" value={starts} id="starts" className="form-control"/>
              <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleEndChange} placeholder="ends" required type="datetime-local" name="ends" value={ends} id="ends" className="form-control"/>
              <label htmlFor="ends">Ends</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleDescriptionChange} placeholder="description" required type="text" name="description" value={description} id="description" className="form-control"/>
              <label htmlFor="description">Description</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlemaxPresentationsChange} placeholder="Max presentations" required type="number" name="Max presentations" value={maxPresentations} id="Max presentations" className="form-control"/>
              <label htmlFor="Max presentations">Max presentations</label>
            </div>            <div className="form-floating mb-3">
              <input onChange={handleMaxAttendeesChange} placeholder="Max attendees" required type="number" name="Max attendees" value={maxAttendees} id="Max attendees" className="form-control"/>
              <label htmlFor="Max attendees">Max attendees</label>
            </div>
            <div className="mb-3">
            <select onChange={handleLocationChange} required name="location" value={location} id="location" className="form-select">
              <option value="">Choose a location</option>
              {locations.map(locations => {
                return (
                    <option key={locations.name} value={locations.name}>{locations.name}</option>
                );
              })}
            </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ConferenceForm;
