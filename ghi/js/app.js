
function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
    <div class="shadow-sm p-3 mb-15 bg-white rounded">
        <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <p class="card-subtitle mb-2 text-muted">${location}</p>
            <p class="card-text">${description}</p>
            <p class="card-text">Starts: ${starts}</p>
            <p class="card-text">Ends: ${ends}</p>
        </div>
        </div>
      </div>`;
}


function createAlert(message) {
  return `
  <div class="alert alert-primary d-flex align-items-center" role="alert">
  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="orange" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
    <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
  </svg>
  <div>
    ${message}
  </div>
  `;
};


function truncate(str, maxlength) {
  return (str.length > maxlength) ?
    str.slice(0, maxlength - 1) + '...' : str;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.log(response);
      } else {
        const data = await response.json();
        const column = document.querySelectorAll('.col');
        let i = 0

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = truncate(details.conference.description, 85);
                const starts = new Date(details.conference.starts).toLocaleDateString();
                const ends = new Date(details.conference.ends).toLocaleDateString();
                const location = details.conference.location.name;
                const pictureUrl = details.conference.location.picture_url;
                const html = createCard(name, description, pictureUrl, starts, ends, location);
                column[i].innerHTML += html;
                if (i === 2) {
                  i = 0
                }
                else {
                  i++
                };
              }
          }
        }
    } catch (e){
      const alert = document.querySelector(".container");
      alert.innerHTML = createAlert(e);
    }
    });
